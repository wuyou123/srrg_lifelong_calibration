#include <srrg_l2c_kinematics/forklift_kinematics.h>
#include <srrg_l2c_gui/trajectory_viewer.h>
#include <srrg_l2c_utils/file_reader.h>


#include <srrg_system_utils/system_utils.h>

using namespace srrg_l2c;

const char* banner [] = {
  "test forklift odom: from nw to valhalla",
  "",
  "usage: test_forklift_odometry -encoder encoder.txt -odom odom.txt",
  "-h       <flag>           this help",
  "-encoder <string>         filename containing encoder counts",
  "-odom    <string>         filename containing odometry",
  0
};


int main(int argc, char** argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;
  std::string encoder_file = "";
  std::string odom_file = "";
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if(!strcmp(argv[c], "-encoder")) {
      c++;
      encoder_file = argv[c];
    } else if(!strcmp(argv[c], "-odom")) {
      c++;
      odom_file = argv[c];
    } 
    c++;
  }

  DatasetPtr dataset_odom = DatasetPtr(new Dataset());
  DatasetPtr dataset_encoder = DatasetPtr(new Dataset());
  
  FileReader filereader;
  
  filereader.setFile(odom_file);
  if(!filereader.compute(*dataset_odom))
    std::runtime_error("[Main]: Error while processing the odometry dataset");  
  filereader.setFile(encoder_file);
  if(!filereader.computeForkliftEncoder(*dataset_encoder))
    std::runtime_error("[Main]: Error while processing the encoder dataset");  
  
  Isometry3 first_forklift_pose = dataset_odom->begin()->second->isometry();

  BaseKinematics* forklift = new ForkliftKinematics();
  forklift->setOdomParams(Vector2(0.8, 0.0));
  
  Vector3 dead_reackoning(first_forklift_pose.translation().x(),
                          first_forklift_pose.translation().y(),
                          0.0);
  dead_reackoning.setZero();

  DatasetPtr dead_reackoning_dataset = DatasetPtr(new Dataset());

  double prev_timestamp = -1;
    
  for(const Sample& sample : *dataset_encoder) {
    const double& current_timestamp = sample.first;
    const MeasurePtr& measure = sample.second;
    if(prev_timestamp < 0.0) {
      prev_timestamp = current_timestamp;
      continue;
    }
    const double relative_timestamp = current_timestamp - prev_timestamp;    
    prev_timestamp = current_timestamp;

    forklift->directKinematics(dead_reackoning,
                               measure->encoderTicks());

    Isometry3 iso_measure;
    iso_measure.translation().head(2) = dead_reackoning.head(2);
    iso_measure.linear() = srrg_core::Rz(dead_reackoning(2));

    dead_reackoning_dataset->insert(Sample(current_timestamp, MeasurePtr(new Measure(iso_measure))));
  }
  
  QApplication app(argc, argv);
  TrajectoryViewer viewer;

  viewer.setDataset(dead_reackoning_dataset);

  viewer.show();

  //  delete forklift;
  
  return app.exec();
  
  
}
