#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_imgui/calibrator_imgui.h>

using namespace srrg_l2c;

const char* banner [] = {
  "lifelong_calibration_gui: from nw to valhalla",
  "",
  "usage: lifelong_calibration_gui -config configuration.yaml",
  "-config  <string>        yaml configuration file, see default.yaml",
  "-h       <flag>          this help",
  0
};


int main(int argc, char** argv) { 
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }
  int c = 1;
  std::string configuration_file = "";  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-config")) {
      c++;
      configuration_file = argv[c];
    }
    c++;
  }

  if(configuration_file.empty())
    throw std::runtime_error("[LifelongCalibrator] specify a YAML configuration file");

  ros::init(argc, argv, "lifelong_calibration_online_node");
  ros::NodeHandle nh("~");
  ROS_INFO("node started");

  CalibratorGUI calibrator(CalibratorRos::ONLINE);
  calibrator.setNodeHandle(nh);
  calibrator.initFromYaml(configuration_file);
  
  //  ros::spin();
  
  return 0;
}
