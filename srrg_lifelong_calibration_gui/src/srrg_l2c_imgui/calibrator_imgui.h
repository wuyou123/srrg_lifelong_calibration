#pragma once

#include <iostream>
#include "imgui/imgui.h"
#include "imgui/imgui_addons.h"
#include <ros/ros.h>

#include <srrg_l2c_ros/calibrator_ros.h>

namespace srrg_l2c {

  namespace Colors {
    static const ImVec4 Green  = ImVec4(0.4f, 0.8f, 0.0f, 1.0f);
    static const ImVec4 Orange = ImVec4(0.8f, 0.4f, 0.0f, 1.0f);
    static const ImVec4 Yellow = ImVec4(0.9f, 0.8f, 0.0f, 1.0f);
  }
  
  class CalibratorGUI : public CalibratorRos{
  public:
    CalibratorGUI(const Mode m_);
    ~CalibratorGUI();

    void showGUI(bool* p_open);

  protected:
    void drawRobotStatus();
    void drawSensorStatus();
    void updateStats();
    
  private:   

    float* _chi_stats;
    int _size_of_stats;
    
  };

}
