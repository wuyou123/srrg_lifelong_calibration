set title 'Error Rotation'
set xlabel 'sequence'
set ylabel 'error'

set grid
set key below center horizontal noreverse enhanced autotitle box dashtype solid
set tics out nomirror
set border 3 front linetype black linewidth 1.0 dashtype solid

set xtics 1
#set mxtics 1

set yrange [0:0.4]
# set ytics 5

set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2

set style histogram clustered gap 1 title offset character 0, 0, 0
set style data histograms

set boxwidth 1.0 absolute
set style fill solid 5.0 border -1

set terminal png enhanced
set output 'rotation.png'

plot 'all_results.txt' using 3:xtic(1) title 'Full', \
	'' using 5 title 'Keivan', \
	'' using 7 title 'DellaCorte'