#!/bin/bash
counter=0
PROSLAM="_imls.txt"
IMU_GT="_imu_gt.txt"
PARAMS_CAM="../calib_cam_to_imu.txt"
BASE_COMMAND="rosrun srrg_lifelong_calibration offline_app_kitti "
KEIVAN="rosrun srrg_lifelong_calibration offline_app_kitti_keivan "
DELLACORTE="rosrun srrg_lifelong_calibration offline_app_kitti_dellacorte "
ODOM=" -odom "
ESTIMATE=" -estimate "
GT=" -gt "
for counter in 0{0..9} 10;
do
    cd $counter
    echo "################################################################################"
    echo "#############             SEQUENCE #$counter                    ######################"
    echo "################################################################################"
    ARGUMENTS="$ODOM $counter$IMU_GT $ESTIMATE $counter$PROSLAM $GT $PARAMS_CAM -o"
    COMMAND1="$BASE_COMMAND $ARGUMENTS"
    COMMAND2="$KEIVAN $ARGUMENTS"
    COMMAND3="$DELLACORTE $ARGUMENTS"
    echo $COMMAND1
    $COMMAND1
    echo $COMMAND2
    $COMMAND2
    echo $COMMAND3
    $COMMAND3
    echo done!
    cd ..
    ((counter++))
done
echo All done
echo Invoking Plot Method
# Now we merge all the results in a single plottable file
out_file="all_results.txt"
rm $out_file
base="output_base.txt"
dellacorte="output_dellacorte.txt"
keivan="output_keivan.txt"
for counter in 0{0..9} 10;
do
    filebase="$counter/$base"
    filekeivan="$counter/$keivan"
    filedellacorte="$counter/$dellacorte"
    echo $counter > tmp.txt
    paste tmp.txt $filebase $filekeivan $filedellacorte >> $out_file
    ((counter++))
done
rm tmp.txt
gnuplot generate_translation.gnu
gnuplot generate_rotation.gnu
eog translation.png rotation.png
