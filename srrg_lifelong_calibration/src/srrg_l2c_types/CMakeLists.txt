add_library(srrg_l2c_types_library STATIC
  sensor.cpp
  measure.cpp
  dataset.cpp
  matrix_info.cpp
)

target_link_libraries(srrg_l2c_types_library
  ${catkin_LIBRARIES}
  srrg_l2c_utils_library
  ${OpenCV_LIBS}
)
