#pragma once
#include "defs.h"
#include <iomanip>
#include <Eigen/Eigenvalues>

namespace srrg_l2c {

  /*! 
   * A small utility to analyze a squared Matrix.
   * It computes eigen{values/vectors}, rank, determinant
   */
  struct MatrixInfo {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /** c'tor */
    MatrixInfo(const MatrixX& m_);   

    /** prints matrix info */
    const void print() const;

    /** writes on file */
    const void write(const std::string& filename_) const;

    /** performs standardization {rad and meters} */
    void standardize();

    /** performs inverse standardization {rad and meters} */
    void standardizeInverse();

    /** computes the matrix info */
    void compute();

    MatrixX matrix;                  // stores the matrix to be analyzed
    Eigen::EigenSolver<MatrixX> eig; // stores the eigen{values/vectors}
    real determinant;                // stores the matrix determinant
    int rank;                        // stores the matrix rank
    real eig_ratio;                  // stores the ratio btw min and max eig
    
  private:
    /** computes standardization  */
    void standardize(MatrixX& matrix);
    
    bool computed = false;            // check if already computed
  };
  
}
