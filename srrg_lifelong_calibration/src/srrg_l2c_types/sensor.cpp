#include "sensor.h"

namespace srrg_l2c {

  Sensor::Prior::Prior() {
    param_names_.insert(std::pair<int, std::string>(PARAM::X,"X"));
    param_names_.insert(std::pair<int, std::string>(PARAM::Y,"Y"));
    param_names_.insert(std::pair<int, std::string>(PARAM::Z,"Z"));
    param_names_.insert(std::pair<int, std::string>(PARAM::QX,"QX"));
    param_names_.insert(std::pair<int, std::string>(PARAM::QY,"QY"));
    param_names_.insert(std::pair<int, std::string>(PARAM::QZ,"QZ"));
    sensor_value_.setIdentity();
    value_.setIdentity();
    inv_value_.setIdentity();
    information_matrix_.setIdentity();
  }
  
  void Sensor::Prior::computeMatrices(Vector6& e,
                              Matrix6& J,
                              Matrix6& Omega){   
    Matrix3 R = inv_value_.linear();
    R.transposeInPlace();
    Omega.setZero();
    
    Omega.block(0,0,3,3) = R * information_matrix_.block(0,0,3,3)*R.transpose();
    Omega.block(3,3,3,3) = R * information_matrix_.block(3,3,3,3)*R.transpose();
    
    e = error();
    const real epsilon = 1e-2;
    const float two_inverse_epsilon = .5/epsilon;
    J.setZero();
    for(size_t i = 0; i < 6 ; ++i){
      Vector6 increment = Vector6::Zero();
      increment(i) = epsilon;
      J.col(i) = two_inverse_epsilon * (error(increment) - error(-increment));
    }        
  }
  
  Vector6 Sensor::Prior::error(const Vector6& delta_x) const {
    Isometry3 delta_X = srrg_core::v2t(delta_x);
    return srrg_core::t2v( inv_value_ * sensor_value_ * delta_X);
  }
  
  void Sensor::Prior::setPrior(const Isometry3& prior_value,
                       PARAM param,
                       const Isometry3& sensor_value,
                       const real& magnitude) {
    value_ = prior_value;
    sensor_value_ = sensor_value;
    inv_value_ = value_.inverse();
    information_matrix_.setZero();
    information_matrix_(param, param) = magnitude;
    param_prior_ = param;
  }
  
  const void Sensor::Prior::print() const {
    std::cerr << FGRN("  - [Prior] Param ")
              << (param_names_.find(param_prior_))->second << std::endl;
  }
  

  void Sensor::Prior::updateSensorValue(const Isometry3& sensor_value) {
    sensor_value_ = sensor_value;
  }

  
  Sensor::PriorPtrVector::~PriorPtrVector() {
    std::cerr << FRED("[PriorVector][destroying] ") << std::endl;
  }
  
  Sensor::Sensor(const std::string& sensor_name) {
    extrinsics_.setIdentity();
    inv_extrinsics_.setIdentity();
    information_matrix_.setIdentity();
    sensor_name_    = sensor_name;
    frame_id_       = "";
    child_frame_id_ = "";
    time_delay_     = 0.0;
    estimate_time_delay_ = false;
  }
    
  Sensor::~Sensor() {
    std::cerr << FRED("[Sensor][destroying] ") << sensor_name_ << std::endl;
    //    priors_.clear();   
  }
  
  void Sensor::setFrames(const std::string& frame_id,
                         const std::string& child_frame_id) {
    frame_id_ = frame_id;
    child_frame_id_ = child_frame_id;
  }

  void Sensor::setInformationMatrix(const Matrix6& information_matrix) {
    information_matrix_ = information_matrix;
  }

  void Sensor::setInformation(const real& information_magnitude) {
    information_matrix_ = information_magnitude * Matrix6::Identity();
  }
  
  void Sensor::setExtrinsics(const Isometry3& init_guess) {
    extrinsics_ = init_guess;
    inv_extrinsics_ = extrinsics_.inverse();
    extrinsics_vector_ = srrg_core::t2v(extrinsics_);
  }
  
  void Sensor::setExtrinsics(const Vector6& init_guess) {
    extrinsics_vector_ = init_guess;
    setExtrinsics(srrg_core::v2t(extrinsics_vector_));
  }
  
  void Sensor::setTimeDelay(const real& time_delay) {
    time_delay_ = time_delay;
  }

  void Sensor::setEstimateTimeDelay(const bool estimate_time_delay) {
    estimate_time_delay_ = estimate_time_delay;
  }

  void Sensor::setCameraMatrix(const Matrix3& camera_matrix) {
    camera_matrix_ = camera_matrix;
  }

  
  void Sensor::addPrior(const real& prior_value,
                        PARAM param,
                        const real& magnitude) {
    if(param > 5)
      throw std::runtime_error("[Sensor][addPrior]: adding Prior out of Sensor size!");
    
    for(const PriorPtr& prior : priors_) {
      if(prior->param_prior_ == param) {
        throw std::runtime_error("This Prior has already been set");
      }
    }
    Vector6 prior = Vector6::Zero();
    prior[param] = prior_value;
    PriorPtr p = PriorPtr(new Prior());
    p->setPrior(srrg_core::v2t(prior), param, extrinsics_, magnitude);
    p->print();
    priors_.push_back(p);
  }
  
  const Isometry3& Sensor::extrinsics() const {return extrinsics_;}

  const Vector6& Sensor::extrinsicsVector() const {return extrinsics_vector_;}
  
  const Isometry3& Sensor::inverseExtrinsics() const {return inv_extrinsics_;}

  const real& Sensor::timeDelay() const {return time_delay_;}
  
  const Matrix6& Sensor::informationMatrix() const {return information_matrix_;}
  
  void Sensor::update(const Isometry3& dx,
                      const real& dx_td) {
    extrinsics_ = extrinsics_ * dx;
    inv_extrinsics_ = extrinsics_.inverse();
    time_delay_ += dx_td;
    extrinsics_vector_ = srrg_core::t2v(extrinsics_);
  }

  const std::string& Sensor::name() const {return sensor_name_;}

  const std::string& Sensor::frameId() const {return frame_id_;}

  const std::string& Sensor::childFrameId() const {return child_frame_id_;}

  const bool Sensor::estimateTimeDelay() const {return estimate_time_delay_;}

  const Matrix3& Sensor::cameraMatrix() const {return camera_matrix_;}
  
  void Sensor::updatePriorsExtrinsics() const {
    for(PriorPtr prior_ptr : priors_) {
      prior_ptr->updateSensorValue(extrinsics_);
    }
  }
  
  const typename Sensor::PriorPtrVector& Sensor::priors() const {
    updatePriorsExtrinsics();
    return priors_;
  }
    
  const void Sensor::print() const {
    std::cerr << BOLD(FGRN("[SENSOR] ")) << sensor_name_ << std::endl
              << FGRN("  - [frames] ") << frame_id_ << " -> " << child_frame_id_
              << std::endl
              << FGRN("  - [Omega diag] ") << information_matrix_.diagonal().transpose()
              << std::endl
              << FGRN("  - [extrinsics] ") << srrg_core::t2v(extrinsics_).transpose()
              << std::endl;
    if(estimate_time_delay_)
      std::cerr << FGRN("  - [time delay] ") << time_delay_
                << std::endl;
      for(const PriorPtr& prior : priors_)
        prior->print();
      std::cerr << RST;
  } 
  
}
