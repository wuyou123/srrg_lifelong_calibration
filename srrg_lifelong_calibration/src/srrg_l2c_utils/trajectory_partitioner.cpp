#include "trajectory_partitioner.h"

namespace srrg_l2c {

  TrajectoryPartitioner::TrajectoryPartitioner() {    
  }
  TrajectoryPartitioner::~TrajectoryPartitioner() {    
  }

  void TrajectoryPartitioner::updateStats(const TrajectoryPortion& portion_) {
    _stats.number_of_portions++;
    _stats.total_duration += portion_.duration;
    if(portion_.status == Measure::Status::Unclassified)
      throw std::runtime_error("[TrajectoryPartitioner][updateStats]: unclassified portion!");
    else if(portion_.status == Measure::Status::StraightConstant) {
      _stats.number_of_straight_constant++;
      _stats.straight_constant_duration += portion_.duration;
    }
    else if(portion_.status == Measure::Status::StraightVariable) {
      _stats.number_of_straight_variable++;
      _stats.straight_variable_duration += portion_.duration;
    }
    else if(portion_.status == Measure::Status::RotateConstant) {
      _stats.number_of_rotate_constant++;
      _stats.rotate_constant_duration += portion_.duration;
    }
    else if(portion_.status == Measure::Status::RotateVariable) {
      _stats.number_of_rotate_variable++;
      _stats.rotate_variable_duration += portion_.duration;
    }
    else if(portion_.status == Measure::Status::ArcConstant) {
      _stats.number_of_arc_constant++;
      _stats.arc_constant_duration += portion_.duration;
    }
    else if(portion_.status == Measure::Status::ArcVariable) {
      _stats.number_of_arc_variable++;
      _stats.arc_variable_duration += portion_.duration;
    }
    else if(portion_.status == Measure::Status::Still) {
      _stats.number_of_still++;
      _stats.still_duration += portion_.duration;
    }
  }

  const void TrajectoryPartitioner::Stats::print() const {
    std::cerr << FYEL(BOLD("[TrajectoryPartitioner][Stats][print]")) << std::endl;
    std::cerr << FYEL("  - portions:           ") << std::setw(6) << number_of_portions
              << FYEL("  | duration:    ") << total_duration << std::endl;
    std::cerr << FYEL("  - straight constant:  ") << std::setw(6) << number_of_straight_constant << std::setw(6)
              << FYEL("  | duration:    ") << straight_constant_duration << std::endl;
    std::cerr << FYEL("  - straight variable:  ") << std::setw(6) << number_of_straight_variable << std::setw(6)
              << FYEL("  | duration:    ") << straight_variable_duration << std::endl;
    std::cerr << FYEL("  - rotate constant:    ") << std::setw(6) << number_of_rotate_constant << std::setw(6)
              << FYEL("  | duration:    ") << rotate_constant_duration << std::endl;
    std::cerr << FYEL("  - rotate variable:    ") << std::setw(6) << number_of_rotate_variable << std::setw(6)
              << FYEL("  | duration:    ") << rotate_variable_duration << std::endl;
    std::cerr << FYEL("  - arc constant:       ") << std::setw(6) << number_of_arc_constant << std::setw(6)
              << FYEL("  | duration:    ") << arc_constant_duration << std::endl;
    std::cerr << FYEL("  - arc variable:       ") << std::setw(6) << number_of_arc_variable << std::setw(6)
              << FYEL("  | duration:    ") << arc_variable_duration << std::endl;
    std::cerr << FYEL("  - still:              ") << std::setw(6) << number_of_still << std::setw(6)
              << FYEL("  | duration:    ") << still_duration << std::endl;
  }
  

  void TrajectoryPartitioner::getSensorPortion(TrajectoryPortion& portion_,
                                               const SensorDatasetMap& sensor_dataset_) {
    const double& start_time = portion_.start_time;
    const double& duration = portion_.duration;
    const double end_time = start_time + duration;
    
    for(const SensorDatasetPair& sensor_datasetptr_pair : sensor_dataset_ ) {
      const std::string& sensor_name = sensor_datasetptr_pair.first;
      const Dataset& dataset = *sensor_datasetptr_pair.second;

      DatasetPtr portion_dataset = DatasetPtr(new Dataset());
      Dataset::const_iterator init = dataset.lower_bound(start_time);
      //TODO  check time distance      
      for(; init != dataset.end(); ++init) {
        if(end_time - init->first < 0)
          break;
        portion_dataset->insert(Sample(init->first, init->second));
      }
      portion_.sensor_dataset.insert(SensorDatasetPair(sensor_name, portion_dataset));
    }
    
  }

  void TrajectoryPartitioner::getDatasetPortion(TrajectoryPortion& portion_,
                                                const Dataset& dataset_) {
    Dataset& portion_dataset = *portion_.dataset;
    const double& start_time = portion_.start_time;
    const double  end_time   = start_time + portion_.duration;
    
    Dataset::const_iterator init = dataset_.lower_bound(start_time);
    for(; init != dataset_.end(); ++init) {
      if(end_time - init->first < 0)
        break;
      portion_dataset.insert(Sample(init->first, init->second));
    }    
  }
  
  void TrajectoryPartitioner::computeNormalized(TrajectoryPortionVector& trajectories_,
                                                Dataset& dataset_,
                                                const SensorDatasetMap& sensor_dataset_) {
    TrajectoryPortionVector tmp_trajectories;
    compute(tmp_trajectories, dataset_, sensor_dataset_);
    const int portions_size = tmp_trajectories.size();
    
    const double time_between_gauges = (double)_config.min_samples / _config.rate;
    const double half_gauges_time = time_between_gauges / 2.0;

    trajectories_.clear();
    trajectories_.resize(portions_size);

    int current = 0;
    double old_gauge_time = tmp_trajectories[0].start_time;
    double last_data_time = tmp_trajectories[portions_size-1].start_time +
      tmp_trajectories[portions_size-1].duration;

    for(size_t i = 0; i < portions_size; ++i) {
      const TrajectoryPortion& portion = tmp_trajectories[i];
      const double gauge = portion.start_time + (portion.duration / 2.0);
      if(gauge - half_gauges_time > old_gauge_time &&
         gauge + half_gauges_time < last_data_time) {
        TrajectoryPortion trajectory_portion;
        trajectory_portion.reset();
        trajectory_portion.status = portion.status;
        trajectory_portion.start_time = gauge - half_gauges_time;
        trajectory_portion.duration = time_between_gauges;
        trajectory_portion.sensor_dataset = sensor_dataset_;
        trajectory_portion.dataset = DatasetPtr(new Dataset());
        getDatasetPortion(trajectory_portion, dataset_);
        trajectories_[current++] = trajectory_portion;
      }
    }
    trajectories_.resize(current); 
  }

  
  void TrajectoryPartitioner::compute(TrajectoryPortionVector& trajectories,
                                      Dataset& dataset_,
                                      const SensorDatasetMap& sensor_dataset_) {
    
    //bdc, analyze the whole trajectory first
    _trajectory_analyzer.compute(dataset_);
       
    trajectories.clear();
    trajectories.resize(dataset_.size());
    int traj_id = 0;

    TrajectoryPortion trajectory_portion;
    trajectory_portion.reset();

    double portion_start_time = 0.0;
    bool new_portion = true;
    //bdc, loop over analyzed trajectory to build the Trajectory portions
    for(const Sample& time_measureptr_pair : dataset_) {
      const double& time            = time_measureptr_pair.first;
      const MeasurePtr& measure_ptr = time_measureptr_pair.second;
      
      if(measure_ptr->status() == Measure::Status::Unclassified ||
         measure_ptr->status() != trajectory_portion.status)
        new_portion = true;
      
      if(new_portion) {
        // check if the old one is a good portion
        if(trajectory_portion.duration > _config.min_duration) {
          getSensorPortion(trajectory_portion, sensor_dataset_);
          trajectories[traj_id++] = trajectory_portion;
          updateStats(trajectory_portion);         
        } 
        
        if(measure_ptr->status() != Measure::Status::Unclassified) {
          portion_start_time = time;
          trajectory_portion.reset();
          trajectory_portion.status = measure_ptr->status();
          trajectory_portion.dataset = DatasetPtr(new Dataset());
          trajectory_portion.dataset->insert(Sample(time, measure_ptr));
          trajectory_portion.number_of_sample++;
          trajectory_portion.start_time = portion_start_time;
          new_portion = false;
        }
        continue;
      }

      trajectory_portion.dataset->insert(Sample(time, measure_ptr));
      trajectory_portion.number_of_sample++;
      trajectory_portion.duration = time - portion_start_time;
      
    }
    trajectories.resize(traj_id);
    
  }
  
}
