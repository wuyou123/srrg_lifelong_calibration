#include "file_reader.h"
#include <iomanip>

namespace srrg_l2c {

  FileReader::FileReader() {
    _filename = "";
  }

  FileReader::~FileReader() {}

  void FileReader::setFile(const std::string& filename_) {
    _filename = filename_;    
  }


  // File format: timestamp x y z qx qy qz qw
  bool FileReader::compute(Dataset& dataset_) {
    if(_filename.empty()){
      std::cerr << KRED << "[FileReader][compute]: set the filename before!"
                << RST << std::endl;
      return false;
    }

    // get file iterator
    std::ifstream file;
    file.open(_filename);
    if(!file.is_open()) {
      std::cerr << KRED << "[FileReader][compute]: unable to read file!"
                << RST << std::endl;      
      return false;
    }
    // variable to write
    Vector6 v; v.setZero();
    long double timestamp = 0.0;
    real qw = 0.0;
    
    std::string line;
    while(!file.eof()) {
      getline(file, line);
      char *pch;
      char *cstr_line = new char[line.length() + 1];
      strcpy(cstr_line, line.c_str());
      pch = std::strtok(cstr_line, " ");
      if(pch == NULL)
        continue;      
      std::istringstream ts(pch);
      if(!(ts >> timestamp))
        std::cerr << "[error]: invalid conversion from string to real" << std::endl;
      //      std::cerr << "ts: " << std::setprecision(20) << timestamp << std::endl;
      pch = std::strtok(NULL, " ");
      for(size_t i = 0; i < 6; ++i) {
        std::istringstream vss(pch);
        if(!(vss >> v(i)))
          std::cerr << "[error]: invalid conversion from string to real" << std::endl;
        pch = std::strtok(NULL, " ");
      }
      std::istringstream qwss(pch);
      if(!(qwss >> qw))
        std::cerr << "[error]: invalid conversion from string to real" << std::endl;

      delete [] cstr_line;
      
      // check quaternion normalization
      if(qw < 0)
        v.tail(3) = -v.tail(3);

      
      MeasurePtr measure(new Measure(srrg_core::v2t(v)));
      dataset_.insert(Sample(timestamp, measure));
    }

    const double mean_dt = (dataset_.rbegin()->first - dataset_.begin()->first) / (double)dataset_.size();
    
    std::cerr << FBLU("[FileReader][compute]| Samples: ") << dataset_.size() << std::endl;
    std::cerr << FBLU("                     | Mean dt: ") <<  mean_dt << std::endl;
    
    return true;
  }

  // File format: 12 values, first 3 rows of a transformation matrix
  bool FileReader::compute12(Dataset& dataset_) {
    if(_filename.empty()){
      std::cerr << KRED << "[FileReader][compute]: set the filename before!"
                << RST << std::endl;
      return false;
    }

    // get file iterator
    std::ifstream file;
    file.open(_filename);
    if(!file.is_open()) {
      std::cerr << KRED << "[FileReader][compute]: unable to read file!"
                << RST << std::endl;      
      return false;
    }
    // variable to write
    Vector6 v; v.setZero();
    long double timestamp = 0.0;
    real qw = 0.0;

    Matrix3 R = Matrix3::Zero();
    Vector3 t = Vector3::Zero();
    while (file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
           >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
           >> R(2,0) >> R(2,1) >> R(2,2) >> t(2))
      {
        Isometry3 T;
        T.linear() = R;
        T.translation() = t;
        MeasurePtr measure(new Measure(T));
        dataset_.insert(Sample(timestamp, measure));
        timestamp += 0.1;
        //std::cerr << FGRN("[FileReader]: ") << srrg_core::t2v(T).transpose() << std::endl;
      }    
    return true;
  }

  bool FileReader::computeForkliftEncoder(Dataset& dataset_) {
    if(_filename.empty()){
      std::cerr << KRED << "[FileReader][compute]: set the filename before!"
                << RST << std::endl;
      return false;
    }
    // get file iterator
    std::ifstream file;
    file.open(_filename);
    if(!file.is_open()) {
      std::cerr << KRED << "[FileReader][compute]: unable to read file!"
                << RST << std::endl;      
      return false;
    }
    // variable to write
    VectorX v; v.setZero(2);
    long double timestamp = 0.0;
    long double old_timestamp = -1.0;
    
    std::string line;
    while(!file.eof()) {
      getline(file, line);
      char *pch;
      char *cstr_line = new char[line.length() + 1];
      strcpy(cstr_line, line.c_str());
      pch = std::strtok(cstr_line, " ");
      if(pch == NULL)
        continue;      
      std::istringstream ts(pch);
      if(!(ts >> timestamp))
        std::cerr << "[error]: invalid conversion from string to real" << std::endl;
      //      std::cerr << "ts: " << std::setprecision(20) << timestamp << std::endl;
      pch = std::strtok(NULL, " ");
      for(size_t i = 0; i < 2; ++i) {
        std::istringstream vss(pch);
        if(!(vss >> v(i)))
          std::cerr << "[error]: invalid conversion from string to real" << std::endl;
        pch = std::strtok(NULL, " ");
      }

      delete [] cstr_line;
      
      if(old_timestamp < 0.0) {
        old_timestamp = timestamp;
        continue;
      }

      v(0) *= (timestamp - old_timestamp);  // from fwd_vel to fwd_drive
      old_timestamp = timestamp;
      
      MeasurePtr measure(new Measure(v, Measure::Relative));
      dataset_.insert(Sample(timestamp, measure));
    }

    const double mean_dt = (dataset_.rbegin()->first - dataset_.begin()->first) / (double)dataset_.size();
    
    std::cerr << FBLU("[FileReader][compute]| Samples: ") << dataset_.size() << std::endl;
    std::cerr << FBLU("                     | Mean dt: ") <<  mean_dt << std::endl;
    
    return true;


  }
  
  
}
