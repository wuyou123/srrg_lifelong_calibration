#pragma once
#include <srrg_l2c_types/dataset.h>
#include "trajectory_analyzer.h"

namespace srrg_l2c {

  typedef std::vector<DatasetPtr> DatasetVector;
  typedef std::vector<SensorDatasetMap> SensorDatasetMapVector;
  
  class TrajectorySplitter {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    enum Mode{SampleNum = 0x0, Motion = 0x1};

    TrajectorySplitter(const Mode mode_ = Mode::SampleNum);
    ~TrajectorySplitter() {}
    
    struct Config {      
      int sample_per_dataset = 100;
      double rate = 10; //Hz
    };

    Config& mutableConfig() {return _config;}
    const Config& config() const {return _config;}

    void setDataset(SensorDatasetMap& dataset_map_);    
    void compute(SensorDatasetMapVector& scored_dataset_vector_);

    static bool getDatasetPortion(Dataset& dataset_portion,
                                  const Dataset& dataset,
                                  const double& time_start,
                                  const double& time_end);
    
  protected:
    void computeBySample(SensorDatasetMapVector& scored_dataset_vector_);
    void computeByMotion(SensorDatasetMapVector& scored_dataset_vector_);
    void getSensorPortion(Dataset& dataset_portion,
                          const Dataset& dataset_,
                          const double& start,
                          const double& end);

  private:
    Config _config;
    Mode _mode;
    SensorDatasetMap _dataset_map;
  };

}
