#pragma once
#include <srrg_l2c_types/sensor.h>
#include <srrg_l2c_kinematics/base_kinematics.h>

namespace srrg_l2c {

  typedef std::vector<SensorPtr> SensorPtrVector;

  typedef std::pair<SensorPtr, Vector6> SensorPtrExtrinsicsPair;
  typedef std::map< SensorPtr, Vector6> SensorPtrExtrinsicsMap;
  
  class ChangeDetector {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    ChangeDetector();
    ~ChangeDetector();

    struct Config {
      float kl_divergence_threshold = 0.5;
    };

    Config& mutableConfig() {return *config_;}
    const Config& config() const {return *config_;}

    void setKinematics(const BaseKinematicsPtr& kinematics_ptr);
    void setSensor(const SensorPtr& sensor_ptr);
    void setCovariance(const MatrixX& init_covariance);
    void init();

    bool compute(real& divergence_value,
                 const MatrixX& current_covariance);
    
  protected:
    void klDivergence(float& d, const MatrixX& current_covariance);
    void getMean(VectorX& mean);
    
  private:
    Config* config_;
    SensorPtrExtrinsicsMap sensor_ptr_extrinsics_map_;
    BaseKinematicsPtr kinematics_ptr_;
    MatrixX covariance_;
    real covariance_determinant_;
    VectorX mean_;
    bool initialized_;
    real k_;
  };
  
}
