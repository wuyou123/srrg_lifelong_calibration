#pragma once
#include <fstream>
#include <srrg_l2c_types/dataset.h>

namespace srrg_l2c {

  class FileReader{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    FileReader();
    ~FileReader();

    void setFile(const std::string& filename_);
    bool compute(Dataset& dataset_);
    bool compute12(Dataset& dataset_);

    bool computeForkliftEncoder(Dataset& dataset_);
    
  private:
    std::string _filename;        
  };
  
}
