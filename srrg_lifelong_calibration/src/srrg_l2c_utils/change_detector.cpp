#include "change_detector.h"

namespace srrg_l2c {

  ChangeDetector::ChangeDetector() {
    config_ = new Config();
    kinematics_ptr_ = nullptr;
    initialized_ = false;
    k_ = 0;
  }

  ChangeDetector::~ChangeDetector() {
    delete config_;
  }

  void ChangeDetector::setKinematics(const BaseKinematicsPtr& kinematics_ptr) {
    kinematics_ptr_ = kinematics_ptr;
  }

  void ChangeDetector::setSensor(const SensorPtr& sensor_ptr) {
    sensor_ptr_extrinsics_map_.insert(std::make_pair(sensor_ptr,
                                                     sensor_ptr->extrinsicsVector()));
  }

  void ChangeDetector::setCovariance(const MatrixX& init_covariance) {
    covariance_ = init_covariance;
    covariance_determinant_ = init_covariance.determinant();
    k_ = (float)covariance_.rows();
    mean_ = VectorX::Zero(k_);
  }

  void ChangeDetector::getMean(VectorX& mean) {
    int sensor_id = 0;
    int time_delay_id = 0;
    for(const SensorPtrExtrinsicsPair& pair : sensor_ptr_extrinsics_map_) {
      mean.block<SE3_DIM, 1>(sensor_id*SE3_DIM + time_delay_id,0) = pair.first->extrinsicsVector();
      ++sensor_id;
      if(pair.first->estimateTimeDelay()) {
        mean(sensor_id*SE3_DIM + time_delay_id) = pair.first->timeDelay();
        ++time_delay_id;
      }
    }
    if(kinematics_ptr_)
      mean.tail(kinematics_ptr_->odomParams().size()) = kinematics_ptr_->odomParams();
  }

  void ChangeDetector::init() {
    if(!sensor_ptr_extrinsics_map_.size())
      throw std::runtime_error("[ChangeDetector][init]: set Sensors before init!");

    getMean(mean_);
    
    initialized_ = true;    
  }

  void ChangeDetector::klDivergence(float& d, const MatrixX& current_covariance) {
    d = 0.f;

    const real current_covariance_determinant = current_covariance.determinant();
    const MatrixX current_information = current_covariance.inverse();
    
    const real trace = (current_information * covariance_).trace();
    VectorX current_mean = VectorX::Zero(k_);
    getMean(current_mean);
    const real w = 1e3;
    std::cerr << "[reference]: " << mean_.transpose() << std::endl;
    std::cerr << "[current]:   " << current_mean.transpose() << std::endl;
    
    VectorX mean_difference = (current_mean - mean_);//*w;
    mean_difference.block(3,0,3,1) *= 10;
    if(mean_difference.rows() > 6)
      mean_difference.tail(3) *= 10;
    const real mean_norm = mean_difference.transpose() * mean_difference;
    const real nat_log = log(current_covariance_determinant / covariance_determinant_);
    
    //d = .5 * ( trace + mean_norm - k_ + nat_log );   
    d = mean_norm;
  } 

  bool ChangeDetector::compute(real& kullback_leibler_div,
                               const MatrixX& current_covariance) {
    if(!initialized_)
      throw std::runtime_error("[ChangeDetector][compute]: first initialize!");
    klDivergence(kullback_leibler_div, current_covariance);
    if(kullback_leibler_div > config_->kl_divergence_threshold) {
      // std::cerr << FRED("[ChangeDetector]: trigger d = ")
      //           << kullback_leibler_div << std::endl;
      return true;
    }
    return false;
  }
  
}
