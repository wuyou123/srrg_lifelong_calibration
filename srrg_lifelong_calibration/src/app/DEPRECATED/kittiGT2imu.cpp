#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_system_utils/colors.h>
#include <srrg_l2c_utils/file_reader.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "kitti_gt_2_imu: move back gt trajectory to imu/gps",
  "",
  "usage: kitti_gt_2_imu -gt <kitti-GT-file> -imu2velo <kitti-imu2velo> -velo2cam <kitti-velo2cam> -o imu_gt.txt",
  "-gt <string>            txt file containing gt",
  "-imu2velo <string>      txt file containing imu 2 velo calibration data",
  "-velo2cam <string>      txt file containing velo 2 cam calibration data",
  "-o <string>             output file",
  "-h <flag>               this help",
  0
};


Isometry3 getTransform(const std::string& filename) {
  std::ifstream file;
  file.open(filename);
  if(!file.is_open()) {
    std::cerr << KRED << "unable to read file: " << filename
              << RST << std::endl;      
    std::exit(0);
  }
  Matrix3 R = Matrix3::Zero();
  Vector3 t = Vector3::Zero();
  file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
       >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
       >> R(2,0) >> R(2,1) >> R(2,2) >> t(2);

  Isometry3 T;
  T.linear() = R;
  T.translation() = t;
  return T;
}

int main(int argc, char** argv){
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;

  std::string gt_file       = "";
  std::string imu2velo_file = "";
  std::string velo2cam_file = "";
  std::string imu_gt_file   = "imu_gt.txt";

  while (c < argc) {
    if(!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if(!strcmp(argv[c], "-gt")) {
      c++;
      gt_file = argv[c];
    } else if(!strcmp(argv[c], "-imu2velo")) {
      c++;
      imu2velo_file = argv[c];
    } else if(!strcmp(argv[c], "-velo2cam")) {
      c++;
      velo2cam_file = argv[c];
    } else if(!strcmp(argv[c], "-o")) {
      c++;
      imu_gt_file = argv[c];
    }     
    c++;
  }

  if(gt_file.empty())
    throw std::runtime_error("Missing -gt file!");
  if(imu2velo_file.empty())
    throw std::runtime_error("Missing -imo2velo file!");
  if(velo2cam_file.empty())
    throw std::runtime_error("Missing -velo2cam file!");

  DatasetPtr gt_dataset = DatasetPtr(new Dataset());
  FileReader filereader;
  filereader.setFile(gt_file);
  if(!filereader.compute12(*gt_dataset))
    throw std::runtime_error("[Main]: Error while processing gt dataset!");

  Isometry3 imu_WRT_velo = getTransform(imu2velo_file);
  Isometry3 velo_WRT_cam = getTransform(velo2cam_file);

  Isometry3 cam_WRT_imu = (imu_WRT_velo * velo_WRT_cam).inverse();
  std::cerr << KRED << "cam WRT imu: " << t2v(cam_WRT_imu).transpose() << RST << std::endl;

  // transform dataset to represent the IMU poses
  gt_dataset->transformInPlace(cam_WRT_imu.inverse());
  gt_dataset->write12(imu_gt_file);
  
  return 0;
}
