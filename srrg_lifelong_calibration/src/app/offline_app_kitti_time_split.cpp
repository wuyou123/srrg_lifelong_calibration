#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_utils/file_reader.h>
#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_utils/trajectory_partitioner.h>
#include <srrg_l2c_utils/trajectory_splitter.h>
#include <srrg_l2c_types/matrix_info.h>

using namespace srrg_l2c;
using namespace srrg_core;

const char* banner [] = {
  "offline_app_kitti: offline app designed for kitti-like data",
  "",
  "usage: offline_app_kitti -odom odom.txt -estimate sensor_est.txt",
  "-odom       <string>    txt file containing odom measurements",
  "-estimate   <string>    txt file containing pose sensor estimate measures",
  "-samples    <int>       number of samples per splitted dataset.",
  "-gt         <string>    txt file containing the actual param",
  "-o          <flag>      generate output file with error wrt gt",
  "-h          <flag>      this help",
  "",
  "note: multiple sensors are supported, e.g. -estimate stereo.txt -estimate velodyne.txt -gt stereo_param.txt -gt velo_param.txt",
  0
};

Isometry3 getTransform(const std::string& filename) {
  std::ifstream file;
  file.open(filename);
  if(!file.is_open()) {
    std::cerr << KRED << "unable to read file: " << filename
              << RST << std::endl;      
    std::exit(0);
  }
  Matrix3 R = Matrix3::Zero();
  Vector3 t = Vector3::Zero();
  file >> R(0,0) >> R(0,1) >> R(0,2) >> t(0)
       >> R(1,0) >> R(1,1) >> R(1,2) >> t(1)
       >> R(2,0) >> R(2,1) >> R(2,2) >> t(2);

  Isometry3 T;
  T.linear() = R;
  T.translation() = t;
  return T;
}

int main(int argc, char** argv){

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 0;
  }

  int c = 1;

  std::string odom_file   = "";
  std::vector<std::string> sensor_files;
  std::vector<std::string> actual_params;
  bool output_results = false;
  int samples_per_dataset = 100;
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-odom")) {
      c++;
      odom_file = argv[c];
    } else if (!strcmp(argv[c], "-estimate")) {
      c++;
      sensor_files.push_back(argv[c]);
    } else if (!strcmp(argv[c], "-gt")) {
      c++;
      actual_params.push_back(argv[c]);
    } else if (!strcmp(argv[c], "-samples")) {
      c++;
      samples_per_dataset = std::atoi(argv[c]);
    } else if (!strcmp(argv[c], "-o")) {
      output_results = true;
    } 
    c++;
  }

  const int number_of_sensor = sensor_files.size();
  std::cerr << FGRN("[Main] Calibrating ") << number_of_sensor
            << FGRN(" sensors") << std::endl;
  
  std::vector<std::string> sensor_names(number_of_sensor, "sensor_");
  for(int i = 0; i < number_of_sensor; ++i)
    sensor_names[i] += std::to_string(i);

  DatasetPtr odometry_dataset = DatasetPtr(new Dataset());
  SensorDatasetMap sensor_datasets;
  for(size_t i = 0; i < number_of_sensor; ++i)    
    sensor_datasets.insert(std::make_pair(sensor_names[i], DatasetPtr(new Dataset())));

  
  FileReader filereader;
  filereader.setFile(odom_file);
  if(!filereader.compute12(*odometry_dataset))
    throw std::runtime_error("[Main]: Error while processing odometry dataset");
  for(size_t i = 0; i < number_of_sensor; ++i) {
    filereader.setFile(sensor_files[i]);
    if(!filereader.compute12(*sensor_datasets.at(sensor_names[i])))
      throw std::runtime_error("[Main]: Error while processing sensor dataset");    
  }
  
  // Analyze the dataset
  SensorDatasetMap odom_dataset_map;
  odom_dataset_map.insert(std::make_pair("odom_data", odometry_dataset));
  
  TrajectorySplitter trajectory_splitter(TrajectorySplitter::SampleNum);
  trajectory_splitter.mutableConfig().sample_per_dataset = samples_per_dataset;
  trajectory_splitter.setDataset(odom_dataset_map);
  SensorDatasetMapVector dataset_vector;
  trajectory_splitter.compute(dataset_vector);
  std::cerr << "Dataset Splitted in " << dataset_vector.size() << " portions"
            << std::endl;
  
  std::vector<SensorPtr> sensors;
  for(size_t i = 0; i < number_of_sensor; ++i) {
    sensors.push_back(SensorPtr(new Sensor(sensor_names[i])));
    const Isometry3 sensor_gt = getTransform(actual_params[i]);
    const Vector6 sensor_v = srrg_core::t2v(sensor_gt);
    // if(i < initial_guesses.size())
    sensors[i]->setExtrinsics(sensor_gt);
    if(i == 0)
      sensors[i]->addPrior(sensor_v(2), Sensor::Z);  //stereo Z-gt
    else if(i == 1)
      sensors[i]->addPrior(sensor_v(2), Sensor::Z); //velo Z-gt
  }

  std::cerr << "Sensors initialized" << std::endl;
  Solver solver;
  for(size_t i = 0; i < number_of_sensor; ++i)
    solver.setSensor(sensors[i]);
  const int iterations = 20; 
  solver.setIterations(iterations);
  solver.setEpsilon(1e-4);
  solver.setEpsilonTime(1e-1);
  
  solver.init();

  std::cerr << "Solver initialized" << std::endl;

  std::vector<MatrixX> H_matrix_vector;
  
  for(size_t i = 0; i < dataset_vector.size(); ++i) {
    solver.compute(dataset_vector[i].find("odom_data")->second, sensor_datasets);
    H_matrix_vector.push_back(solver.H());
  }
  //  solver.stats().print();
  std::cerr << "Solver done" << std::endl;
  
  for(size_t i = 0; i < number_of_sensor; ++i)
    sensors[i]->print();

  std::ofstream output_file;
  if(output_file)
    output_file.open("output_base.txt");
  
  for(size_t i = 0; i < actual_params.size(); ++i) {
    // compute estimation error
    const Isometry3 actual_extrinsics =  getTransform(actual_params[i]);
    const Isometry3 extrinsics_error = actual_extrinsics.inverse() * sensors[i]->extrinsics();
    std::cerr << KRED << sensor_names[i] << RST << std::endl;
    std::cerr << FRED("gt [extrinsics]: ") << srrg_core::t2v(actual_extrinsics).transpose() << std::endl;
    const double error_t = extrinsics_error.translation().norm();
    const double error_r = computeAngle(extrinsics_error.linear());
    std::cerr << FRED(" err_t: ") << error_t
              << FRED(" err_r: ") << error_r << std::endl;
    output_file << error_t << " " << error_r << std::endl;    
  }

  // write out on file the values of H matrix
  for(size_t i = 0; i < H_matrix_vector.size(); ++i) {
    MatrixInfo current_H(H_matrix_vector[i]);
    current_H.compute();
    output_file << current_H.determinant << " " << current_H.eig_ratio
                << " " << current_H.determinant*current_H.eig_ratio
                << std::endl;
  }
  
  if(output_file)
    output_file.close();

  return 0;
}
