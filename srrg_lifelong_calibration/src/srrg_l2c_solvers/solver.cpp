#include "solver.h"
#include <iomanip>

namespace srrg_l2c {

  Solver::Solver() {
    kinematics_ptr_        = nullptr;
    reference_dataset_ptr_ = nullptr;
    sensor_dataset_ptr_    = nullptr;
    odom_params_size_    = 0;
    linear_system_index_ = 0;
    odom_params_index_   = 0;
    cumulative_size_     = 0;
    verbose_           = false;
    initialized_       = false;
    initialized_time_  = false;
    time_delay_locked_ = false;
    epsilon_      = 1e-7;
    epsilon_time_ = 1e-4;
    iterations_   = 10;
    stats_.chi_evolution.resize(iterations_);
    stats_.outliers_evolution.resize(iterations_);
    stats_.inliers_evolution.resize(iterations_);
  }
  
  Solver::~Solver() {
    // iterate over the map and delete the linearizers
    if(verbose_)
      std::cerr << FRED("[Solver][destroying]") << std::endl;
    for(const StringLinearizerPtrPair& pair : linearizers_) {
      if(verbose_)
        std::cerr << "linearizer connected with " << KRED
                  << pair.first
                  << RST << std::endl;
      //delete pair.second.linearizer_ptr; //now is a shared ptr
    }
  }

  const void Solver::Stats::print() const {
    for(size_t iteration = 0; iteration < chi_evolution.size(); iteration++)
      std::cerr << FBLU("iteration: ") << std::setw(5) << iteration
                << FBLU(" inliers: ") << std::setw(5) << inliers_evolution[iteration]
                << FBLU(" outliers: ") << std::setw(5) << outliers_evolution[iteration]
                << FBLU(" chi: ") << chi_evolution[iteration] << std::endl;
  }
  
  void Solver::setKinematics(const BaseKinematicsPtr& kinematics_ptr) {
    if(!kinematics_ptr)
      throw std::runtime_error("[Solver][setKinematics]: NULL Ptr to Kinematics");

    kinematics_ptr_ = kinematics_ptr;
    odom_params_size_ = (kinematics_ptr_->odomParams()).size();    
  }

  void Solver::setIterations(const int iterations) {
    iterations_ = iterations;
    stats_.chi_evolution.resize(iterations);
    stats_.outliers_evolution.resize(iterations);
    stats_.inliers_evolution.resize(iterations);
  }
  
  void Solver::setEpsilon(const real& epsilon) {
    epsilon_ = epsilon;
  }

  void Solver::setEpsilonTime(const real& epsilon_time) {
    epsilon_time_ = epsilon_time;
  }
  
  void Solver::setCorrelation(const SensorPtr& sensor_ref, const SensorPtr& sensor_est) {
    if(!sensor_est || !sensor_ref)
      throw std::runtime_error("[Solver][setCorrelation]: null ptr!");

    StringLinearizerPtrMap::iterator est_it = linearizers_.find(sensor_est->name());
    StringLinearizerPtrMap::iterator ref_it = linearizers_.find(sensor_ref->name());
    
    if(est_it == linearizers_.end() ||
       ref_it == linearizers_.end()) {
      std::cerr << FRED("[Solver][setCorrelation]: the sensors ")
                << sensor_ref->name()
                << FRED(" and ")
                << sensor_est->name()
                << FRED(" have not been inserted yet!")
                << std::endl;
      return;
    }

    CrossLinearizerPtr cross_linearizer = CrossLinearizerPtr(new CrossLinearizer());
    cross_linearizer->setSensor(sensor_est);
    cross_linearizer->setReferenceSensor(sensor_ref);
    IndexedLinearizer indexed_linearizer;
    indexed_linearizer.linearizer_ptr = cross_linearizer;
    indexed_linearizer.row_id = ref_it->second.row_id;
    indexed_linearizer.row_size = ref_it->second.row_size + est_it->second.row_size;
    indexed_linearizer.col_id = est_it->second.col_id;
    indexed_linearizer.col_size = est_it->second.col_size + est_it->second.col_size;
    indexed_linearizer.dataset_set = new bool(false);
    indexed_linearizer.estimate_time_delay = false;
    
    std::string mangled_name = mangleStrings(sensor_ref->name(), sensor_est->name());
    linearizers_.insert(StringLinearizerPtrPair(mangled_name, indexed_linearizer));

    if(verbose_)
      std::cerr << "[Solver][setCorrelation] added Correlation between sensors: "
                << KBLU
                << sensor_ref->name()
                << " and "
                << sensor_est->name()
                << RST << std::endl;    
  }

  void Solver::setSensor(const SensorPtr& sensor) {
    if(!sensor)
      throw std::runtime_error("[Solver][setSensor]: null ptr!");
    
    if(linearizers_.find(sensor->name()) != linearizers_.end()) {
      std::cerr << FRED("[Solver][setSensor]: the sensor ")
                << sensor->name()
                << FRED(" has already been inserted!")
                << std::endl;
      return;
    }
    
    LinearizerPtr linearizer = LinearizerPtr(new Linearizer());
    linearizer->setSensor(sensor);
    IndexedLinearizer indexed_linearizer;
    indexed_linearizer.linearizer_ptr = linearizer;
    indexed_linearizer.row_id = linear_system_index_;
    indexed_linearizer.col_id = linear_system_index_;
    indexed_linearizer.row_size = SE3_DIM;
    indexed_linearizer.col_size = SE3_DIM;
    indexed_linearizer.dataset_set = new bool(false);
    indexed_linearizer.estimate_time_delay = sensor->estimateTimeDelay();
        
    linear_system_index_ += SE3_DIM;
    // bdc, if the sensor need a time_delay estimate, notify it
    if(sensor->estimateTimeDelay()) {
      linear_system_index_ += 1;
      indexed_linearizer.row_size = SE3_DIM + 1;
      indexed_linearizer.col_size = SE3_DIM + 1;
    }

    linearizers_.insert(StringLinearizerPtrPair(sensor->name(), indexed_linearizer));

    if(verbose_)
      std::cerr << "[Solver][setSensor] added Sensor called: " << KBLU
                << sensor->name() << RST << std::endl;
  }


  const typename Solver::Stats& Solver::stats() const {
    return stats_;
  }

  const MatrixX& Solver::H() const {
    if(!initialized_time_)
      throw std::runtime_error("[Solver][H]: missing initialization!");
    return H_;
  }

  
  void Solver::setVerbosity(const bool verbose) {
    verbose_ = verbose;
  }

  void Solver::setDatasets(const DatasetPtr& reference_dataset,
                           const SensorDatasetMapPtr& sensor_dataset) {
    setReferenceDataset(reference_dataset);
    setSensorDataset(sensor_dataset);
  }
  void Solver::setReferenceDataset(const DatasetPtr& reference_dataset_ptr) {
    if(!reference_dataset_ptr)
      throw std::runtime_error("[Solver][setReferenceDatasets]: nullptr here!");
    reference_dataset_ptr_ = reference_dataset_ptr;
  }
  void Solver::setSensorDataset(const SensorDatasetMapPtr& sensor_dataset_ptr) {
    if(!sensor_dataset_ptr)
      throw std::runtime_error("[Solver][setSensorDatasets]: nullptr here!");
    sensor_dataset_ptr_ = sensor_dataset_ptr;
  }
  
  void Solver::lockTimeDelayEstimate(const bool lock) {
    time_delay_locked_ = lock;
    for(const StringLinearizerPtrPair& pair : linearizers_)
      pair.second.linearizer_ptr->lockTimeDelayEstimate(lock);
  }
  

  void Solver::init() {
    const int number_of_sensor = linearizers_.size();
    
    //compute complessive dimension
    cumulative_size_ = odom_params_size_ +  linear_system_index_;

    odom_params_index_ = linear_system_index_;
    
    H_ = MatrixX::Zero(cumulative_size_, cumulative_size_);
    b_ = VectorX::Zero(cumulative_size_);
    dx_ = VectorX::Zero(cumulative_size_);

    if(verbose_)
      std::cerr << "[Solver][init] size of problem: " << cumulative_size_ << std::endl;
    
    for(const StringLinearizerPtrPair& pair_string_linearizer : linearizers_) {      
      BaseLinearizerPtr base_linearizer = (pair_string_linearizer.second.linearizer_ptr);
      base_linearizer->setEpsilon(epsilon_);
      base_linearizer->setEpsilonTime(epsilon_time_);
      base_linearizer->setVerbosity(verbose_);

      // check if standard Linearizer
      LinearizerPtr linearizer = std::dynamic_pointer_cast<Linearizer>(base_linearizer);
      if(linearizer) {
        if(kinematics_ptr_)
          linearizer->setKinematics(kinematics_ptr_);
        linearizer->init();
        continue;
      }      
      // check if cross Linearizer
      CrossLinearizerPtr cross_linearizer = std::dynamic_pointer_cast<CrossLinearizer>(base_linearizer);      
      if(cross_linearizer) {
        cross_linearizer->init();
        continue;
      }             
    }

    lockTimeDelayEstimate(time_delay_locked_);    
    initialized_time_ = true;
  }


  void Solver::sumMatricesStdLinearizer(const MatrixX& H_block,
                                        const VectorX& b_block,
                                        const IndexedLinearizer& il) {
    // sensor-sensor
    H_.block(il.row_id,
             il.col_id,
             il.row_size,
             il.col_size) += H_block.block(0,0,il.row_size, il.col_size);
    b_.block(il.row_id, 0,
             il.row_size, 1) += b_block.block(0, 0, il.row_size, 1);
   
    
    if(!odom_params_size_)
      return;
    H_.block(odom_params_index_,
             odom_params_index_,
             odom_params_size_,
             odom_params_size_) += H_block.block(il.row_size, il.col_size ,odom_params_size_, odom_params_size_);
    b_.block(odom_params_index_, 0,
             odom_params_size_, 1) += b_block.block(il.row_size, 0, odom_params_size_, 1);
    H_.block(odom_params_index_,
             il.col_id,
             odom_params_size_,
             il.col_size) += H_block.block(il.row_size, 0, odom_params_size_, il.col_size);
    // sensor-odom
    H_.block(il.row_id,
             odom_params_index_,
             il.row_size,
             odom_params_size_) += H_block.block(0, il.col_size, il.row_size, odom_params_size_);      
  }


  void Solver::sumMatricesCrossLinearizer(const MatrixX& H_block,
                                          const VectorX& b_block,
                                          const IndexedLinearizer& il) {
    // first add the self-sensor components
    H_.block(il.row_id,
             il.row_id,
             SE3_DIM,
             SE3_DIM) += H_block.block(0, 0, SE3_DIM, SE3_DIM);
    H_.block(il.col_id,
             il.col_id,
             SE3_DIM,
             SE3_DIM) += H_block.block(SE3_DIM, SE3_DIM, SE3_DIM, SE3_DIM);    

    // then add the cross-sensor components
    H_.block(il.row_id,
             il.col_id,
             SE3_DIM,
             SE3_DIM) += H_block.block(0, SE3_DIM, SE3_DIM, SE3_DIM);
    H_.block(il.col_id,
             il.row_id,
             SE3_DIM,
             SE3_DIM) += H_block.block(SE3_DIM, 0, SE3_DIM, SE3_DIM);    

    // Now b components
    // TODO check this that won't work if using time delay inside cross linearizers
    
    b_.block(il.row_id, 0, SE3_DIM, 1) += b_block.head(SE3_DIM);
    b_.block(il.col_id, 0, SE3_DIM, 1) += b_block.tail(SE3_DIM);
    
  }



  void Solver::sumMatrices(const MatrixX& H_block,
                           const VectorX& b_block,
                           const IndexedLinearizer& il) {

    LinearizerPtr standard_linearizer = std::dynamic_pointer_cast<Linearizer>(il.linearizer_ptr);
    if(standard_linearizer) {
      sumMatricesStdLinearizer(H_block, b_block, il);
      return;
    }

    CrossLinearizerPtr cross_linearizer = std::dynamic_pointer_cast<CrossLinearizer>(il.linearizer_ptr);
    if(cross_linearizer) {
      sumMatricesCrossLinearizer(H_block, b_block, il);
      return;
    }
    
  }

  void Solver::updateParameters() {

    // update Kinematics params, if any
    if(odom_params_size_) {
      kinematics_ptr_->update(dx_.block(odom_params_index_, 0,
                                        odom_params_size_,  1));
    }
    // update Sensors
    for(const StringLinearizerPtrPair& pair : linearizers_) {
      LinearizerPtr linearizer = std::dynamic_pointer_cast<Linearizer>(pair.second.linearizer_ptr);
      if(!linearizer)
        continue;
      if(linearizer->sensor()->estimateTimeDelay())
        linearizer->sensor()->update(srrg_core::v2tEuler((Vector6)dx_.block(pair.second.row_id, 0, SE3_DIM, 1)), dx_(pair.second.row_id + SE3_DIM));
      else
        linearizer->sensor()->update(srrg_core::v2tEuler((Vector6)dx_.block(pair.second.row_id, 0, SE3_DIM, 1)));
    }          

    
  }

  void Solver::setDatasetToLinearizers(const DatasetPtr& reference_dataset_ptr,
                                       const SensorDatasetMap& sensor_dataset) {
    // bdc, set dataset to linearizers
    for(const StringLinearizerPtrPair& string_id_lin : linearizers_){
      const IndexedLinearizer& id_lin = string_id_lin.second;
      *(id_lin.dataset_set) = false;
      std::string sensor_name1 = "", sensor_name2 = "";
      DatasetPtr data1 = nullptr, data2 = nullptr;            
      if(std::dynamic_pointer_cast<Linearizer>(id_lin.linearizer_ptr)){
        SensorDatasetMap::const_iterator data_it = sensor_dataset.find(string_id_lin.first);
        if(data_it == sensor_dataset.end())
          continue;
        id_lin.linearizer_ptr->setReferenceDataset(reference_dataset_ptr);
        id_lin.linearizer_ptr->setSensorDataset(data_it->second);
        *(id_lin.dataset_set) = true;
      } else if(std::dynamic_pointer_cast<CrossLinearizer>(id_lin.linearizer_ptr)) {
        if(unMangleStrings(sensor_name1, sensor_name2, string_id_lin.first)) {
          SensorDatasetMap::const_iterator data1_it = sensor_dataset.find(sensor_name1);
          if(data1_it != sensor_dataset.end()) {
            data1 = data1_it->second;
          } else
            continue;
          SensorDatasetMap::const_iterator data2_it = sensor_dataset.find(sensor_name2);
          if(data2_it != sensor_dataset.end()) {
            data2 = data2_it->second;
            id_lin.linearizer_ptr->setReferenceDataset(data1);
            id_lin.linearizer_ptr->setSensorDataset(data2);            
            *(id_lin.dataset_set) = true;
          } else
            continue;
        }
      }
      
    }
  }
  

  void Solver::callLinearizers(const int iteration) {
    // bdc, They are initialized based on different sizes
    MatrixX H_block;
    VectorX b_block;     
    const int size = SE3_DIM + odom_params_size_;
    const int size_w_time = SE3_DIM + odom_params_size_ + 1;    

    for(const StringLinearizerPtrPair& string_id_lin : linearizers_){
      const IndexedLinearizer& id_lin = string_id_lin.second;
      if(!*(id_lin.dataset_set))
        continue;
      if(id_lin.estimate_time_delay) {
        // bdc, this SUCKS
        H_block.setZero(size_w_time, size_w_time);
        b_block.setZero(size_w_time);
      } else {
        H_block.setZero(size, size);
        b_block.setZero(size);
      }
      id_lin.linearizer_ptr->linearize(H_block,
                                       b_block,
                                       stats_.chi_evolution[iteration],
                                       stats_.inliers_evolution[iteration],
                                       stats_.outliers_evolution[iteration]);
      sumMatrices(H_block, b_block,id_lin);
    }
  }
  
  void Solver::compute() {
    if(!initialized_time_)
      throw std::runtime_error("[Solver][compute]: missing initialization!");
    if(!reference_dataset_ptr_ || !sensor_dataset_ptr_)
      throw std::runtime_error("[Solver][compute]: datasets are NULL");

    TrajectoryPortionVector trajectory_portion_vector;
    TrajectoryPartitioner trajectory_partitioner;
    trajectory_partitioner.compute(trajectory_portion_vector,
                                   *reference_dataset_ptr_,
                                   *sensor_dataset_ptr_);
    trajectory_partitioner.stats().print();

   
    for(size_t iteration = 0; iteration < iterations_; ++iteration) {
      H_.setZero();
      b_.setZero();
      dx_.setZero();
      stats_.chi_evolution[iteration] = 0.0;
      stats_.inliers_evolution[iteration] = 0;
      stats_.outliers_evolution[iteration] = 0;
      int portion_id = 0;
      for(const TrajectoryPortion& portion : trajectory_portion_vector) {
        // if(portion.status != Measure::Status::ArcConstant &&
        //    portion.status != Measure::Status::ArcVariable)
        //   continue;

        //bdc,  set dataset to linearizers
        setDatasetToLinearizers(portion.dataset, portion.sensor_dataset);
        //bdc,  call linearizers
        callLinearizers(iteration);
        portion_id++;
      }
      //bdc, solver linear system
      dx_ = H_.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(-b_);
     
      //bdc, update Parameters
      updateParameters();
      
    } // end iterations
    
  }
  

  
  void Solver::compute(const DatasetPtr& reference_dataset_ptr,
                       const SensorDatasetMap& sensor_dataset) {
    if(!initialized_time_)
      throw std::runtime_error("[Solver][compute]: missing initialization!");
    
    // bdc, set dataset to linearizers
    setDatasetToLinearizers(reference_dataset_ptr, sensor_dataset);   

    for(size_t iteration = 0; iteration < iterations_; ++iteration) {
      H_.setZero();
      b_.setZero();
      dx_.setZero();
      stats_.chi_evolution[iteration] = 0.0;
      stats_.inliers_evolution[iteration] = 0;
      stats_.outliers_evolution[iteration] = 0;
      
      //bdc, call linearizers to compose linear system
      callLinearizers(iteration);
      
      //bdc, solver linear system
      dx_ = H_.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(-b_);
      
      //bdc, update Parameters
      updateParameters();
      
    } // end iterations
    
  }
   
}
