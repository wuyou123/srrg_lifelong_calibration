#include "cross_linearizer.h"

namespace srrg_l2c {

  CrossLinearizer::CrossLinearizer() : BaseLinearizer() {
    error_size_ = 3;
    current_estimate_.setIdentity();
    kernel_threshold_ = .3;
  }
  
  void CrossLinearizer::init() {
    if(!sensor_ptr_ || !reference_sensor_ptr_)
      throw std::runtime_error("[Linearizer][init]: missing sensors!");
    total_param_size_ = 6;
    if(sensor_ptr_->estimateTimeDelay()){
      estimate_time_delay_ = true;
      //bdc, add a row for the time delay estimate
      total_param_size_ += 1;
    }    
    initialized_ = true;
  }

  void CrossLinearizer::linearize(MatrixX& H,
                                  VectorX& b,
                                  real& chi,
                                  int& inliers,
                                  int& outliers) {
    if(!initialized_)
      throw std::runtime_error("[Linearizer][linarize]: missing initialization!");
    //bdc, TODO fix this shit
    H.setZero(SE3_DIM*2, SE3_DIM*2);
    b.setZero(SE3_DIM*2);
    real current_chi = 0;
    int current_outliers = 0, current_inliers = 0;
    
    VectorX current_b = VectorX::Zero(SE3_DIM*2);
    MatrixX current_H = MatrixX::Zero(SE3_DIM*2, SE3_DIM*2);
    int analyzed_data = 0;

    // get isom of first measure
    Isometry3 iso_start = sensor_dataset_ptr_->begin()->second->isometry();
    double time_start = sensor_dataset_ptr_->begin()->first;

    //get current estimate
    current_estimate_ = reference_sensor_ptr_->extrinsics().inverse() * sensor_ptr_->extrinsics();
    
    for(const Sample& time_measure : *sensor_dataset_ptr_) {
      const Isometry3& iso_current = time_measure.second->isometry();
      Isometry3 iso_relative = iso_start.inverse() * iso_current;
      if(!checkDistance(iso_relative, transition_threshold_, rotation_threshold_))
        continue;
      iso_start = iso_current;
      const double& time_current = time_measure.first;
      
      if(hessianAndResidual(current_H, current_b, current_chi,
                            current_inliers,
                            current_outliers,
                            time_measure.second,
                            time_current)) {
        
        H += current_H;
        b += current_b;
        ++analyzed_data;       
      }     

      time_start = time_current;
    }
    
    chi += (current_chi/analyzed_data);
    outliers += (current_outliers/analyzed_data);
    inliers += (current_inliers/analyzed_data);

    H /= analyzed_data;
    b /= analyzed_data;
    
    // Vector prior_e;
    // Matrix prior_J;
    // J.setZero();
    // Matrix prior_Omega;
    // for(size_t i = 0; i < (_sensor->priors()).size(); ++i) {
    //   _sensor->priors().at(i)->computeMatrices(prior_e, prior_J, prior_Omega);
    //   J.block(0,0,DIM, DIM) = prior_J;
    //   H_ += J.transpose() * prior_Omega * J;
    //   b_ += J.transpose() * prior_Omega * prior_e;
    // }
    
  }

  
  
  bool CrossLinearizer::hessianAndResidual(MatrixX& H,
                                           VectorX& b,
                                           real& chi,
                                           int& inliers,
                                           int& outliers,
                                           const MeasurePtr& sensor_measure,
                                           const double& time) {

    double effective_time = time;
    effective_time += (sensor_ptr_->timeDelay() - reference_sensor_ptr_->timeDelay());

    const MeasurePtr& reference_measure = reference_dataset_ptr_->nearest(effective_time);
    if(reference_measure == reference_dataset_ptr_->end()->second ||
       reference_measure == reference_dataset_ptr_->begin()->second) {
      return false;
    }

    const KDTree::VectorTDVector& points = sensor_measure->points();
    const KDTree::VectorTDVector& refpoints = reference_measure->points();
    const KDTree& kd_tree = reference_measure->kdTree();
   
    const real max_distance = 0.5;

    for(const Vector3& point : points) {
      
      const Vector3 query = current_estimate_ * point;
      srrg_core::KDTree<real, POINT_SIZE>::VectorTD answer;
      
      //bdc,  query the tree
      int index = -1;
      real approx_distance = kd_tree.findNeighbor(answer,
                                                  index,
                                                  query,
                                                  max_distance);

      if(index < 0)
        continue;

      // bdc, from here on all the computations are made in the reference sensor frame,
      // same identical result can be achieved expressing everything in a common
      // reference frame, say the odometry. What matters is the relative computation
      // of error and Jacobian
      
      // compute Error
      Vector3 e = query - answer;      
      
      const real current_chi = e.transpose() * e;      
      real scale = 1.0;
      if(current_chi > kernel_threshold_) {
        scale *= sqrt(kernel_threshold_ / current_chi);
        outliers++;
      }
      
      chi += current_chi;
      
      // compute Jacobian wrt sensor
      MatrixX J2 = MatrixX::Zero(POINT_SIZE, SE3_DIM);
      const Matrix3 minus_p_moving_cross=srrg_core::skew((const Vector3&)-query);
      J2.block<3,3>(0,0).setIdentity();
      J2.block<3,3>(0,3) = minus_p_moving_cross;
      
      Matrix3 Omega = Matrix3::Identity();
      // TODO here add Time delay column
      
      MatrixX J1 = MatrixX::Zero(POINT_SIZE, SE3_DIM);
      const Matrix3 minus_p_reference_cross=srrg_core::skew((const Vector3&)-answer);
      J1.block<3,3>(0,0).setIdentity();
      J1.block<3,3>(0,3) = minus_p_reference_cross;
      J1 *= -1;     

      // see "A Tutorial on Graph-Bases SLAM", Grisetti et al. 2010     
      const MatrixX H11 = J1.transpose() * Omega * scale * J1;
      const VectorX b1  = J1.transpose() * Omega * scale * e;      
      const MatrixX H22 = J2.transpose() * Omega * scale * J2;
      const VectorX b2  = J2.transpose() * Omega * scale * e;
      const MatrixX H12 = J1.transpose() * Omega * scale * J2;
      const MatrixX H21 = H12.transpose();
      
      H.block<SE3_DIM, SE3_DIM>(0,             0) += H11;
      H.block<SE3_DIM, SE3_DIM>(0,       SE3_DIM) += H12;
      H.block<SE3_DIM, SE3_DIM>(SE3_DIM, SE3_DIM) += H22;
      H.block<SE3_DIM, SE3_DIM>(SE3_DIM,       0) += H21;

      b.head(SE3_DIM) += b1;
      b.tail(SE3_DIM) += b2;
      ++inliers;
    }

    return true;
  }

  
}
