#ifndef _L2C_LINEARIZER_H_
#define _L2C_LINEARIZER_H_

#include "base_linearizer.h"

namespace srrg_l2c {
   
  /*!
   * Linearizer class for odometry-encoder vs sensor trajectory
   * problems.
   */
  class Linearizer : public BaseLinearizer {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    
    /** empty c'tor */
    Linearizer();

    /** empty d'tor */
    ~Linearizer() {}
    
    /** initialized the linearizer */
    virtual void init() override;    

    /** linearizes the nonlinear input problem */
    virtual void linearize(MatrixX& H,
                           VectorX& b,
                           real& chi,
                           int& inliers,
                           int& outliers) override;
    
  protected:
    
    /** computes the perturbated error */
    void errorWithPerturbation(VectorX& e,
                               const MeasurePtr& reference_measure_ptr,
                               const MeasurePtr& sensor_measure_ptr,
                               const int index,
                               const real& perturbation);

    /** computes error and Jacobian invoking the errorWithPerturbation method */
    virtual bool errorAndJacobian(VectorX& e,
                                  MatrixX& J,     
                                  const MeasurePtr& relative_sensor_measure,
                                  const double& time,
                                  const double& period = 0.0) override;

    /** computes numerically the time delay jacobian */
    virtual bool timeDelayJacobian(VectorX& J_col,
                                   const MeasurePtr& relative_sensor_measure,
                                   const double& period,
                                   const double& effective_time) override;    
  };

  /** shared pointer of the Linearizer */
  typedef std::shared_ptr<Linearizer> LinearizerPtr;

  /** const shared pointer of the Linearizer */
  typedef std::shared_ptr<Linearizer const> LinearizerConstPtr;
  
}

#endif
