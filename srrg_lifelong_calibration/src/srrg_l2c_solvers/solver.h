#ifndef _L2C_SOLVER_H_
#define _L2C_SOLVER_H_

#include <srrg_types/types_mat.hpp>
#include <srrg_system_utils/colors.h>

#include <srrg_l2c_utils/trajectory_partitioner.h>
#include <srrg_l2c_types/matrix_info.h>

#include "linearizer.h"
#include "cross_linearizer.h"

namespace srrg_l2c {

  /*!
   * Manages the location in the full linear system
   * of the related linearizer_ptr
   */
  struct IndexedLinearizer{
    int row_id;
    int col_id;
    int row_size;
    int col_size;
    bool estimate_time_delay;
    bool *dataset_set;
    BaseLinearizerPtr linearizer_ptr;
  };

  /** pair of string and Linearizer for sensor_name indexing */
  typedef std::pair<std::string, IndexedLinearizer> StringLinearizerPtrPair;  

  /** map of string and Linearizer for sensor_name indexing */
  typedef std::map<std::string, IndexedLinearizer> StringLinearizerPtrMap;
  

  /*!
   * The Solver class is used to describe the nonlinear problem.
   * It allocates the needed linearizers and solve the linear system returned by them
   */
  class Solver {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /*!
     * error Evolution statistics of the Solver
     */
    struct Stats {
      std::vector<real> chi_evolution;     // evolution of the chi^2 during optimization
      std::vector<int> inliers_evolution;  // evolution of the number of inliers
      std::vector<int> outliers_evolution; // evolution of the number of outliers
      /** print the solver statistics */
      const void print() const;
    };
    
    /** empty c'tor */
    Solver();

    /** empty d'tor */
    ~Solver();
    
    /** sets the kinematics (if any) */
    void setKinematics(const BaseKinematicsPtr& kinematics_ptr);

    /** sets a sensor pointer */
    void setSensor(const SensorPtr& sensor_ptr);
    
    /** sets a correlation between two sensors */
    void setCorrelation(const SensorPtr& sensor_ref_ptr,
                        const SensorPtr& sensor_est_ptr);

    /** sets the epsilon used for numeric jacobian */
    void setEpsilon(const real& epsilon);

    /** sets the epsilon used for numeric jacobian of the time delay*/
    void setEpsilonTime(const real& epsilon_time);
    
    /** sets the verbosity */
    void setVerbosity(const bool verbose);

    /** sets the maximum number of iterations */
    void setIterations(const int iterations);

    /** sets the datasets */
    void setDatasets(const DatasetPtr& reference_dataset_ptr,
                     const SensorDatasetMapPtr& sensor_dataset_ptr);

    /** sets the reference dataset (may be either encoder-counts or isometries) */
    void setReferenceDataset(const DatasetPtr& reference_dataset_ptr);

    /** sets the sensor dataset  */
    void setSensorDataset(const SensorDatasetMapPtr& sensor_dataset_ptr);

    /** gets const reference to Stats */
    const Stats& stats() const;

    /** gets const reference to Hessian matrix H_ */
    const MatrixX& H() const;    

    /** initializes the solver */
    void init();
    
    /** computes the calibration after the set Datasets calls */
    void compute();

    /** computes the calibration  */
    void compute(const DatasetPtr& reference_dataset_ptr,
                 const SensorDatasetMap& sensor_dataset);

    /** lock the estimate of time delay for all the linearizers  */
    void lockTimeDelayEstimate(const bool lock);
    
  protected:

    /** assembles the linear system for the odom-sensor linearizers */
    void sumMatricesStdLinearizer(const MatrixX& H_block,
                                  const VectorX& b_block,
                                  const IndexedLinearizer& il);

    /** assembles the linear system for the sensor-sensor linearizers */
    void sumMatricesCrossLinearizer(const MatrixX& H_block,
                                    const VectorX& b_block,
                                    const IndexedLinearizer& il);

    /** sums linearized blocks to the whole system */
    void sumMatrices(const MatrixX& H_block,
                     const VectorX& b_block,
                     const IndexedLinearizer& il);

    /** sets the datasets to the relative linearizers */
    void setDatasetToLinearizers(const DatasetPtr& reference_dataset_ptr,
                                 const SensorDatasetMap& sensor_dataset);

    /** invokes all the linearizers */
    void callLinearizers(const int iteration);

    /** performs the parameter update */
    void updateParameters();    

  private:

    BaseKinematicsPtr kinematics_ptr_;       // pointer to kinematics (if any)
    DatasetPtr reference_dataset_ptr_;       // pointer to reference dataset
    SensorDatasetMapPtr sensor_dataset_ptr_; // pointer to sensors datasets   
    StringLinearizerPtrMap linearizers_;     // map of linearizers 
    Stats stats_;                            // statistics of the solver
    int odom_params_size_;                   // size of odom parameters (if any)
    int cumulative_size_;                    // full size of the linear problem
    int iterations_;                         // max number of iterations
    int linear_system_index_;                // current linear system index
    int odom_params_index_;                  // index of odometry parameter in system
    MatrixX H_;                              // Hessian matrix
    VectorX b_;                              // Residual vector
    VectorX dx_;                             // solution of the optimization problem
    real epsilon_;                           // epsilon value for numeric jacobian
    real epsilon_time_;                      // epsilon value for numeric jacobian of time delay
    bool time_delay_locked_;                 // flag to indicate the lock of time delay estaimte
    bool verbose_;                           // verbosity flag
    bool initialized_;                       // init flag
    bool initialized_time_;                  // init time flag 
  };

  /** shared pointer of Solver */
  typedef std::shared_ptr<Solver> SolverPtr;
  /** const shared pointer of Solver */
  typedef std::shared_ptr<Solver const> SolverConstPtr;
  
}

#endif
