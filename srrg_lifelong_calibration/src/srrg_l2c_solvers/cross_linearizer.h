#ifndef _L2C_CROSS_LINEARIZER_H_
#define _L2C_CROSS_LINEARIZER_H_

#include "base_linearizer.h"

namespace srrg_l2c {
   
  /*!
   * Linearizer class for sensor vs sensor estimate
   * problems.
   */
  class CrossLinearizer : public BaseLinearizer {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    
    /** empty c'tor */
    CrossLinearizer();

    /** empty d'tor */
    ~CrossLinearizer() {}
    
    /** initialized the linearizer */
    virtual void init() override;

    /** linearizes the nonlinear input problem */
    virtual void linearize(MatrixX& H,
                           VectorX& b,
                           real& chi,
                           int& inliers,
                           int& outliers) override;

    
  protected:
    
    /** computes the linearization */
    bool hessianAndResidual(MatrixX& H,
                            VectorX& b,
                            real& chi,
                            int& inliers,
                            int& outliers,
                            const MeasurePtr& relative_sensor_measure,
                            const double& time);

  private:

    int error_size_;              // full size of the error
    Isometry3 current_estimate_;  // stores the current estimate
  };
  
  /** shared pointer of CrossLinearizer */
  typedef std::shared_ptr<CrossLinearizer> CrossLinearizerPtr;

  /** const shared pointer of CrossLinearizer */
  typedef std::shared_ptr<CrossLinearizer const> CrossLinearizerConstPtr;
  
}

#endif
