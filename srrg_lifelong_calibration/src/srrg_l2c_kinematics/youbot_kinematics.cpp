#include "youbot_kinematics.h"

namespace srrg_l2c {

  void YoubotKinematics::directKinematics(Vector3 &pose,
                                          const VectorX &encoder_ticks){
    directKinematics(pose, encoder_ticks, odom_params_);
  }

  void YoubotKinematics::directKinematics(Vector3 &pose,
                                          const VectorX &encoder_ticks,
                                          const VectorX &odom_params){
    
    real geom_factor = odom_params(0);
    real w_FL = odom_params(1);
    real w_FR = odom_params(2);
    real w_BL = odom_params(3);
    real w_BR = odom_params(4);

    // factory kinematics
    //    real delta_longitudinal = (-encoder_ticks(0)*w_FL + encoder_ticks(1)*w_FR - encoder_ticks(2)*w_BL + encoder_ticks(3)*w_BR)/4;
    //    real delta_trasversal = (encoder_ticks(0)*w_FL + encoder_ticks(1)*w_FR - encoder_ticks(2)*w_BL - encoder_ticks(3)*w_BR)/4;
    //    pose(2) += (encoder_ticks(0)*w_FL + encoder_ticks(1)*w_FR + encoder_ticks(2)*w_BL + encoder_ticks(3)*w_BR)/(4*geom_factor);

    // vrep compliant kinematics
    real delta_longitudinal = (-encoder_ticks(0)*w_FL - encoder_ticks(1)*w_FR - encoder_ticks(2)*w_BL - encoder_ticks(3)*w_BR)/4;
    real delta_trasversal = (encoder_ticks(0)*w_FL - encoder_ticks(1)*w_FR - encoder_ticks(2)*w_BL + encoder_ticks(3)*w_BR)/4;
    pose(2) += (encoder_ticks(0)*w_FL - encoder_ticks(1)*w_FR + encoder_ticks(2)*w_BL - encoder_ticks(3)*w_BR)/(4*geom_factor);

    srrg_core::normalizeAngle(pose(2));
    pose(0) = pose(0) + delta_longitudinal*cos(pose(2)) - delta_trasversal*sin(pose(2));
    pose(1) = pose(1) + delta_longitudinal*sin(pose(2)) + delta_trasversal*cos(pose(2));
  }

  const void YoubotKinematics::print() const {
    std::cerr << KGRN
              << "[OdomParams]: "
              << "baseline_ratio: " << RST
              << odom_params_(0) << " "
              << KGRN
              << "w_FL: " << RST
              << odom_params_(1) << " "
              << KGRN
              << "w_FR: " << RST
              << odom_params_(2) << " "
              << KGRN
              << "w_BL: " << RST
              << odom_params_(3) << " "
              << KGRN
              << "w_BR: " << RST
              << odom_params_(4)
              << std::endl;      
  }
  
}
