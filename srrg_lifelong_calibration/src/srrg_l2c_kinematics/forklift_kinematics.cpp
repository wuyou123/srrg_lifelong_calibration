#include "forklift_kinematics.h"

namespace srrg_l2c {

  void ForkliftKinematics::directKinematics(Vector3& pose,
                                            const VectorX& input_data) {
    directKinematics(pose, input_data, odom_params_);
  }

  void ForkliftKinematics::directKinematics(Vector3& pose,
                                            const VectorX& input_data,
                                            const VectorX& odom_params) {

    const real& drive_dist = input_data(0); // odom_drive_vel * time_diff
    const real& odom_alpha = input_data(1);

    // const real& l = odom_params(0);
    // const real& a = odom_params(1);   
    // real& pose_x     = pose(0);
    // real& pose_y     = pose(1);    
    // real& pose_theta = pose(2);    
    // const real cos_a = cos(odom_alpha);
    // const real sin_a = sin(odom_alpha);    
    // pose_x += drive_dist * (cos_a + a*sin_a/l) * cos_a;
    // pose_x += drive_dist * (cos_a + a*sin_a/l) * sin_a;
    // pose_theta += drive_dist * sin_a / l;
    // srrg_core::normalizeAngle(pose_theta);
    
    real dd = 0.0, da = 0.0;
    if(std::fabs(odom_alpha) < 1e-6) {
      dd = drive_dist;
      da = 0;
    } else {
      const real& w_Dx = odom_params(0);
      const real& w_Dy = odom_params(1);   
      const real r_stear = w_Dx / sin(odom_alpha);
      const real r_fix   = r_stear * cos(odom_alpha) + w_Dy;		
      dd = r_fix / r_stear * drive_dist; 
      da = drive_dist / r_stear;
    }
    real& pose_x     = pose(0);
    real& pose_y     = pose(1);    
    real& pose_theta = pose(2);    
    const real dx = dd * cos(pose_theta + da / 2. );
    const real dy = dd * sin(pose_theta + da / 2. );
    pose_x     += dx;
    pose_y     += dy;
    pose_theta += da;
    srrg_core::normalizeAngle(pose_theta);

  }

  void ForkliftKinematics::update(const VectorX& dx) {
    odom_params_ += dx;
  }

  const void ForkliftKinematics::print() const {
    std::cerr << KGRN
              << "[OdomParams]: "
              << "w_Dx: " << RST
              << odom_params_(0) << " "
              << KGRN
              << "w_Dy: " << RST
              << odom_params_(1)
              << std::endl;
  }
    

}
