#pragma once
#include "ros/ros.h"
#include <sensor_msgs/JointState.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_listener.h>
#include <srrg_l2c_types/defs.h>

namespace srrg_l2c {

  void nav2iso(Isometry3& iso, const nav_msgs::OdometryConstPtr &odom);

  template<typename Transform>
    void transformTFToEigenImpl(const tf::Transform &t, Transform &e) {
    for (int i = 0; i < 3; i++) {
      e.matrix()(i, 3) = t.getOrigin()[i];
      for (int j = 0; j < 3; j++) {
        e.matrix()(i, j) = t.getBasis()[i][j];
      }
    }
    // Fill in identity in last row
    for (int col = 0; col < 3; col++)
      e.matrix()(3, col) = 0;
    e.matrix()(3, 3) = 1;
  }

}
