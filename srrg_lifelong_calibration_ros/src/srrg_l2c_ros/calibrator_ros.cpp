#include "calibrator_ros.h"

namespace srrg_l2c {

  CalibratorRos::CalibratorRos(const Mode m_) {
    _mode = m_;
    _robot = nullptr;
    _reference_dataset = DatasetPtr(new Dataset());
    _number_of_encoder = 0;    
    _joint_topic = "";
    _odom_topic = "";    
    _yaml_parser = new YamlParser();
    _yaml_filename = "";
    _bag_file = "";
    _block_size = 1000;
    _block_id = 1;
    _sample_counter = 0;
    _rate = 20; // Hz
    _i_rate = 1. / _rate;
    _first_message = true;
    _tf_found = false;
    _initialized = false;
    _previous_odometry.setIdentity();
  }

  CalibratorRos::~CalibratorRos() {
    std::cerr << std::endl
              << FRED("[CalibratorRos]: destroying") << std::endl;
    delete _yaml_parser;
    std::cerr << FRED("[CalibratorRos]: destroyed") << std::endl;

    if(_mode == OFFLINE)
      return;
    //    _string_dataset_map.clear();
  }

  void CalibratorRos::setNodeHandle(ros::NodeHandle nh_) {
    if(_mode != Mode::ONLINE)
      throw std::runtime_error("[CalibratorRos][setNodeHandle]: this method is supported only in ONLINE mode!");   
    _nh = nh_;    
  }

  void CalibratorRos::setSensors() {
    size_t se_i = 0;
    for(SensorPtr sensor : _sensor_ptr_vector) {
      DatasetPtr dataset = DatasetPtr(new Dataset());
      _string_dataset_map.insert(SensorDatasetPair(sensor->name(), dataset));
      FramesHandle frame_handle(sensor->name(), sensor->frameId(), sensor->childFrameId());
      _frames_handle_vector[se_i++] = frame_handle;   
      _solver.setSensor(sensor);
    }
    
  }
  
  void CalibratorRos::initFromYaml(const std::string& yaml_file_) {
    _yaml_filename = yaml_file_;
    _yaml_parser->setYamlFile(yaml_file_);
    _yaml_parser->parse(_sensor_ptr_vector);
    _yaml_parser->print();

    _joint_topic = _yaml_parser->jointTopic();
    _odom_topic  = _yaml_parser->odomTopic();
    
    const int sensor_num = _sensor_ptr_vector.size();
    _frames_handle_vector.resize(sensor_num);    
    setSensors();
    
    _solver.setIterations(_yaml_parser->iterations());
    _solver.setEpsilon(1e-4);
    const real dt = 1.0 / _yaml_parser->rate();
    std::cerr << FGRN("dt: ") << dt << std::endl;
    _solver.setEpsilonTime(dt);    

    if(_mode == OFFLINE) {
      _bag_loader.setJointTopic(_joint_topic);
      _bag_loader.setOdomTopic(_odom_topic);
      _bag_loader.setFramesHandlerVector(_frames_handle_vector);
    }      
    
    if(!_joint_topic.empty()) {
      if(_mode == ONLINE) {
        _ref_subscriber = _nh.subscribe(_joint_topic, 1, &CalibratorRos::jointStateCallback, this);
        std::cerr << "subscribed to: " << _joint_topic << std::endl;
      }
      // todo clean this considering several kinematics
      _robot = DifferentialDriveKinematicsPtr(new DifferentialDriveKinematics());
      _number_of_encoder = 2;
      _robot->setOdomParams(_yaml_parser->robotIntrinsics());
      _solver.setKinematics(_robot);
    } else if(!_odom_topic.empty() && _mode == ONLINE) {
      _ref_subscriber = _nh.subscribe(_odom_topic, 1, &CalibratorRos::odomCallback,
                                     this);
      std::cerr << "subscribed to: " << _odom_topic << std::endl;
    }
    
    _block_size = _yaml_parser->blockSize();
    std::cerr << "block size: " << _block_size << std::endl;
    _rate = _yaml_parser->rate();
    _i_rate = 1. / _rate;

    _solver.init();
    _initialized = true;
  }

  void CalibratorRos::calibrate() {
    _solver.compute(_reference_dataset,
                    _string_dataset_map);
    std::cerr << std::endl;
    _solver.stats().print();
    if(_robot)
      _robot->print();
    for(const SensorPtr& sensor : _sensor_ptr_vector)
      sensor->print();        
  }
  
  void CalibratorRos::calibrateIfEnoughData() {
    bool do_calibration = false;
    for(SensorDatasetPair string_data_pair : _string_dataset_map)
      if(string_data_pair.second->size() >= _block_size*_block_id) {
        ++_block_id;
        do_calibration = true;
      }
    if(do_calibration) calibrate();
  }
  
  void CalibratorRos::generateSample(const double& timestamp_) {
    _tf_found = false;
    std::cerr << FBLU("\rSamples ") << ++_sample_counter << std::flush;        
    for(FramesHandle& fh : _frames_handle_vector) {
      MeasurePtr measure(new Measure(fh.current_iso));
      _string_dataset_map.find(fh.name)->second->insert(Sample(timestamp_, measure));
    }  
  }
  
  void CalibratorRos::odomCallback(const nav_msgs::OdometryConstPtr &odom_) {
    Isometry3 current_odometry;
    nav2iso(current_odometry, odom_);
    // look for sensors tf
    lookupTransforms();
    if(_first_message && _tf_found) {
      _first_timestamp = odom_->header.stamp;
      _last_processed = _first_timestamp;
      _previous_odometry = current_odometry;
      _first_message = false;
    } else if (_tf_found) {
      if( (odom_->header.stamp - _last_processed).toSec() < _i_rate)
          //        || !checkDistance(_previous_odometry.inverse()*current_odometry, 0.1, 0.1))
        return;
      else {
        _last_processed = odom_->header.stamp;
        _previous_odometry = current_odometry;
      }
      double current_timestamp = (odom_->header.stamp - _first_timestamp).toSec();
      generateSample(current_timestamp);
      MeasurePtr odom_measure(new Measure(current_odometry));
      _reference_dataset->insert(Sample(current_timestamp, odom_measure));
    }
    calibrateIfEnoughData();
  }
  
  void CalibratorRos::jointStateCallback(const sensor_msgs::JointStateConstPtr &joint_state_) {
    VectorX current_ticks(_number_of_encoder);
    for(size_t i = 0; i < _number_of_encoder; ++i)
      current_ticks(i) = (float)joint_state_->position[i];
    // look for sensors tf
    lookupTransforms();    
    if(_first_message && _tf_found) {
      _first_timestamp = joint_state_->header.stamp;
      _last_processed = _first_timestamp;
      _first_message = false;
    } else if (_tf_found) {
      if( (joint_state_->header.stamp - _last_processed).toSec() < _i_rate)
        return;
      else
        _last_processed = joint_state_->header.stamp;
      double current_timestamp = (joint_state_->header.stamp - _first_timestamp).toSec();
      generateSample(current_timestamp);
      MeasurePtr joint_measure(new Measure(current_ticks));
      _reference_dataset->insert(Sample(current_timestamp, joint_measure));       
    }
    calibrateIfEnoughData();    
  }
  
  void CalibratorRos::lookupTransforms() {
    tf::StampedTransform current_T;
    _tf_found = true;
    for(FramesHandle& fh : _frames_handle_vector) {
      try {
        _tf_listener.lookupTransform(fh.frame_id, fh.child_frame_id, ros::Time(0), current_T);
        transformTFToEigenImpl(current_T, fh.current_iso);
      }
      catch (tf::TransformException &ex) {
        _tf_found = false;
        //std::cerr << KRED << "." << RST;
        ROS_ERROR("%s", ex.what());
      }
    }
  }

  void CalibratorRos::compute() {
    if(!_initialized)
      throw std::runtime_error("[CalibratorRos][compute]: missing initialization!");
    _bag_loader.load(*_reference_dataset, _string_dataset_map);

    // _reference_dataset->print();
    // std::exit(1);
    
    calibrate();
    
  }
  
  void CalibratorRos::setBagFile(const std::string& bag_file_) {
    if(_mode != OFFLINE)
      throw std::runtime_error("[CalibratorRos][setBagFile] this method is supported only in OFFLINE mode!");
    _bag_file = bag_file_;
    _bag_loader.setBagFile(_bag_file);
  }
  
}
