#pragma once

#include <iostream>
#include <srrg_l2c_types/sensor.h>
#include <yaml-cpp/yaml.h>

namespace srrg_l2c {

  typedef std::vector<SensorPtr> SensorPtrVector;
  
  class YamlParser {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    YamlParser();
    YamlParser(const std::string& yaml_file);
    ~YamlParser();
    
    void setYamlFile(const std::string& yaml_file_);
    void parse(SensorPtrVector& sensor_ptr_vector);
    
    const void print() const;
    
    const std::string& jointTopic() const;
    const std::string& odomTopic() const;
    const std::string& robotType() const;
    const int blockSize() const;
    const double& rate() const;
    const int iterations() const;
    const VectorX& robotIntrinsics() const;
    
  private:
    std::string _joint_topic;
    std::string _odom_topic;
    std::string _robot_type;
    int _block_size;
    double _rate;
    int _iterations;
    VectorX _robot_intrinsics;
    
    YAML::Node _configuration;    
  };

  
} // end namespace
