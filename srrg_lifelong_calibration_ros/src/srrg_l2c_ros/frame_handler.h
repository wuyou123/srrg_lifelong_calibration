#pragma once

#include <iostream>
#include <Eigen/Geometry>

namespace srrg_l2c {

  struct FramesHandle {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    FramesHandle();
    FramesHandle(const std::string& name_,
                 const std::string& frame_id_,
                 const std::string& child_frame_id_);
    FramesHandle(const FramesHandle& c);

    const void print() const;
  
    std::string name;
    std::string frame_id;
    std::string child_frame_id;
    Eigen::Isometry3f old_iso;
    Eigen::Isometry3f current_iso;
  };

  typedef std::vector<FramesHandle, Eigen::aligned_allocator<FramesHandle> > FramesHandlerVector;

}
