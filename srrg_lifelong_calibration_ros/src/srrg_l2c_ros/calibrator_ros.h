#pragma once

#include "ros_utils.h"
#include "frame_handler.h"
#include "yaml_parser.h"
#include "bag_loader.h"

#include <srrg_l2c_solvers/solver.h>
#include <srrg_l2c_kinematics/differential_drive_kinematics.h>

#include <Eigen/Geometry>

namespace srrg_l2c {

  class CalibratorRos {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    enum Mode {ONLINE = 0x0, OFFLINE = 0x1};
    
    CalibratorRos(const Mode m_);
    ~CalibratorRos();
    
    void initFromYaml(const std::string& yaml_file_);
    
    // for ONLINE mode
    void setNodeHandle(ros::NodeHandle nh_);
    // for OFFLINE mode
    void setBagFile(const std::string& bag_file_);
    void compute();
    
  protected:
    void odomCallback(const nav_msgs::OdometryConstPtr& odom_);
    void jointStateCallback(const sensor_msgs::JointStateConstPtr& joint_state_);
    void lookupTransforms();
    void setSensors();                    
    void generateSample(const double& timestamp_);
    void calibrateIfEnoughData();
    void calibrate();
    
    Mode _mode;
    FramesHandlerVector _frames_handle_vector;
    SensorDatasetMap _string_dataset_map;
    DatasetPtr _reference_dataset;

    BaseKinematicsPtr _robot;
    int _number_of_encoder;

    std::string _yaml_filename;
    YamlParser* _yaml_parser;
    BagLoader _bag_loader;
    ros::NodeHandle _nh;
    ros::Subscriber _ref_subscriber;
    tf::TransformListener _tf_listener;

    std::string _joint_topic;
    std::string _odom_topic;

    std::string _bag_file;
    
    int _block_size;
    int _block_id;
    int _sample_counter;
    real _rate;
    real _i_rate;
    bool _first_message;
    bool _tf_found;
    bool _initialized;
    ros::Time _last_processed;
    ros::Time _first_timestamp;

    Isometry3 _previous_odometry;
    
    SensorPtrVector _sensor_ptr_vector;

    Solver _solver;
    
  };
  
}
