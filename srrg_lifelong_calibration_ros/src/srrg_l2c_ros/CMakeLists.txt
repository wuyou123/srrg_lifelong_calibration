add_library(srrg_l2c_ros_library SHARED
  calibrator_ros.h calibrator_ros.cpp
  ros_utils.h ros_utils.cpp
  frame_handler.h frame_handler.cpp
  yaml_parser.h   yaml_parser.cpp
  bag_loader.h    bag_loader.cpp
)

target_link_libraries(srrg_l2c_ros_library
  yaml-cpp
  ${catkin_LIBRARIES}
  ${OpenCV_LIBS}
  )
