#include "ros_utils.h"

namespace srrg_l2c {

  void nav2iso(Isometry3& iso, const nav_msgs::OdometryConstPtr &odom){
    Vector6 v;
    v(0) = odom->pose.pose.position.x;
    v(1) = odom->pose.pose.position.y;
    v(2) = odom->pose.pose.position.z;
    v(3) = odom->pose.pose.orientation.x;
    v(4) = odom->pose.pose.orientation.y;
    v(5) = odom->pose.pose.orientation.z;
    iso = srrg_core::v2t(v);
  }

}
