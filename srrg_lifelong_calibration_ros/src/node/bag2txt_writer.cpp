#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_ros/calibrator_ros.h>

#include <algorithm>

using namespace srrg_l2c;

const char* banner [] = {
  "bag2txt_writer: from nw to valhalla",
  "",
  "usage: offline_node -topic <topic_name> bagfile.bag",
  "-topic  <string>         name of topic to read from bag and write",
  "-h       <flag>          this help",
  0
};


int main(int argc, char** argv) {
  
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;
  std::string topic_name = "";
  std::string bag_file = "";
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-topic")) {
      c++;
      topic_name = argv[c];
    } else {
      bag_file = argv[c];
    }
    c++;
  }

  if(topic_name.empty())
    throw std::runtime_error("[Bag2Txt_Writer] specify a topic");

  // TODO: remove ros_init dependency
  ros::init(argc, argv, "lifelong_calibration_bag2txt_node");
  ros::NodeHandle nh("~");
  ROS_INFO("node started");

  BagLoader bag_loader(bag_file);
  bag_loader.setOdomTopic(topic_name);

  Dataset dataset;
  SensorDatasetMap dummy;
  
  bag_loader.load(dataset, dummy);
  
  std::string output_filename = topic_name + ".txt";
  output_filename.erase(std::remove(output_filename.begin(),
                                    output_filename.end(),
                                    '/'), output_filename.end());

  dataset.write(std::string(output_filename));
  
  return 0;
}
